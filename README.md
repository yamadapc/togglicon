# togglicon
Font for Toggl icons. This will give you the Toggl Logo as a font:
```html
<span class="togglicon-logo"></span>
```
